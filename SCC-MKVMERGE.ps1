Add-Type -AssemblyName PresentationFramework
Add-Type -AssemblyName System.WIndows.Forms



#$xamlfile = "d:\Developer\MKVMERGE\WpfApp1\WpfApp1\MainWindow.xaml"
$xamlfile = "MainWindow.xaml" 



$inputXAML=Get-Content -Path $xamlfile -Raw
$inputXAML=$inputXAML -replace 'mc:Ignorable="d"','' -replace "x:N","N" -replace '^<Win.*','<Window'
[XML]$XAML=$inputXAML

$reader = New-Object System.Xml.XmlNodeReader $XAML
try {
    $psform=[WIndows.Markup.XamlReader]::Load($reader)
}catch{
    Write-Host $_.Exception
    throw
}

$xaml.SelectNodes("//*[@Name]") | ForEach-Object {
    try{
        Set-Variable -Name "var_$($_.Name)" -Value $psform.Findname($_.Name) -ErrorAction Stop
        }catch{
            throw
        }
    }

Get-Variable var_*
# Instalace PSGallery a Get-MediaInfo
[Net.ServicePointManager]::SecurityProtocol = [Net.ServicePointManager]::SecurityProtocol -bor [Net.SecurityProtocolType]::Tls12
#Register-PSRepository -Default -Verbose


Try {
    Import-Module PowerShellGet -Force -Erroraction stop
    Write-Host "Modul PSGallery je nainstalován"
        
    
}
Catch {
    Set-PSRepository -Name "PSGallery" -InstallationPolicy Trusted
    Write-Host "Bude instalován modul PowershellGet"
    Install-Module -Name PowershellGet
        
   
}

Try {
    Import-Module Get-MediaInfo -Force -Erroraction stop
    Write-Host "Modul Get-MediaInfo je nainstalován"
            
    }
Catch {
    Write-Host "Bude nainstalován modul Get-MediaInfo"
    Set-PSRepository -Name "PSGallery" -InstallationPolicy Trusted
    Install-Module -Name Get-MediaInfo 
        
   
    }

#Funkce Get-Randomword náhodné heslo
function Get-RandomPassword {
    param (
        [Parameter(Mandatory)]
        [int] $length,
        [int] $amountOfNonAlphanumeric = 1
    )
    Add-Type -AssemblyName 'System.Web'
    return [System.Web.Security.Membership]::GeneratePassword($length, $amountOfNonAlphanumeric)
}
$RandomPASS = Get-RandomPassword 20

#MEDIAINFO

function MEDIAINFO{


$var_default.IsSelected = $true
$Directory = $var_txtSetFolder.Text
Set-Location -path $Directory

$File = Get-ChildItem $var_txtSetFolder.Text -Recurse -File | where Length -gt 1024kb
$i=1 

$var_Progbar.Value = 0  

if($File.count -eq ""){

    $oReturn=[System.Windows.Forms.MessageBox]::Show("Nebyli nalezeny žádné soubory ke zpracování",[System.Windows.Forms.MessageBoxButtons]::"OK") 
    }
    ELSE{

    if($Directory -eq ""){
    $oReturn=[System.Windows.Forms.MessageBox]::Show("Není nastaven vstupní adresář",[System.Windows.Forms.MessageBoxButtons]::"OK")
    
    }ELSE{
    
  
$File = Get-ChildItem $Directory -Recurse -File | where Length -gt 1024kb | Select-Object -First 1

#Get-MediaInfoSummary $FILE |Out-File -filepath $Directory\Process.txt


$ageList = @{
    Czech = 'cs' ; Afrikaans='af';Arabic='ar';Armenian='hy';Basque='eu';Belarusian='be';Tibetan='bo';Bosnian='bs';Breton='br';Bulgarian='bg';Catalan='ca';Chinese='zh';Corsican='co';Welsh='cy';Danish='da';German='de';Dutch='nl';English='en';Esperanto='eo';Estonian='et';Finnish='fi';French='fr';Georgian='ka';Irish='ga';Galician='gl';Hebrew='he';Hindi='hi';Croatian='hr';Hungarian='hu';Icelandic='is';Indonesian='id';Italian='it';Javanese='jv';Japanese='ja';Kannada='kn';Korean='ko';Kurdish='ku';Latin='la';Latvian='lv';Lithuanian='lt';Luxembourgish='lb';Macedonian='mk';Malayalam='ml';Maori='mi';Malay='ms';Mongolian='mn';Nepali='ne';Flemish='nl';Norwegian='no';Polish='pl';Portuguese='pt';Romanian='ro';Russian='ru';Slovak='sk';Slovenian='sl';Spanish='es';Albanian='sq';Sardinian='sc';Serbian='sr';Swedish='sv';Tahitian='ty';Tatar='tt';Telugu='te';Thai='th';Turkish='tr';Ukrainian='uk';Urdu='ur';Vietnamese='vi';Volapük='vo';Walloon='wa';Yiddish='yi'   
}



#Audio Jazyk  $ageList['Czech']
$var_txtLangID1.Text = Get-MediaInfoValue $FILE -Kind Audio -Index 0 -Parameter 'Language/String'
$var_txtLangID1.Text = $ageList[$var_txtLangID1.Text]
if($var_txtLangID1.Text -eq ""){
$var_txtLangID1.Text = "UND"
}
$var_txtLangID2.Text = Get-MediaInfoValue $FILE -Kind Audio -Index 1 -Parameter 'Language/String'
$var_txtLangID2.Text = $ageList[$var_txtLangID2.Text]
if($var_txtLangID2.Text -eq ""){
$var_txtLangID2.Text = "UND"
}
$var_txtLangID3.Text = Get-MediaInfoValue $FILE -Kind Audio -Index 2 -Parameter 'Language/String'
$var_txtLangID3.Text = $ageList[$var_txtLangID3.Text]
if($var_txtLangID3.Text -eq ""){
$var_txtLangID3.Text = "UND"
}
$var_txtLangID4.Text = Get-MediaInfoValue $FILE -Kind Audio -Index 3 -Parameter 'Language/String'
$var_txtLangID4.Text = $ageList[$var_txtLangID4.Text]
if($var_txtLangID4.Text -eq ""){
$var_txtLangID4.Text = "UND"
}
$var_txtLangID5.Text = Get-MediaInfoValue $FILE -Kind Audio -Index 4 -Parameter 'Language/String'
$var_txtLangID5.Text = $ageList[$var_txtLangID5.Text]
if($var_txtLangID5.Text -eq ""){
$var_txtLangID5.Text = "UND"
}

#Audio TrackName
$var_txtTrackNameID1.Text = Get-MediaInfoValue $FILE -Kind Audio -Index 0 -Parameter 'Title'
$var_txtTrackNameID2.Text = Get-MediaInfoValue $FILE -Kind Audio -Index 1 -Parameter 'Title'
$var_txtTrackNameID3.Text = Get-MediaInfoValue $FILE -Kind Audio -Index 2 -Parameter 'Title'
$var_txtTrackNameID4.Text = Get-MediaInfoValue $FILE -Kind Audio -Index 3 -Parameter 'Title'
$var_txtTrackNameID5.Text = Get-MediaInfoValue $FILE -Kind Audio -Index 4 -Parameter 'Title'



#Titulky Jazyk
$var_txtSubID1.Text = Get-MediaInfoValue $FILE -Kind Text -Index 0 -Parameter 'Language/String'
$var_txtSubID1.Text = $ageList[$var_txtSubID1.Text]
if($var_txtSubID1.Text -eq ""){
$var_txtSubID1.Text = "UND"
}
$var_txtSubID2.Text = Get-MediaInfoValue $FILE -Kind Text -Index 1 -Parameter 'Language/String'
$var_txtSubID2.Text = $ageList[$var_txtSubID2.Text]
if($var_txtSubID2.Text -eq ""){
$var_txtSubID2.Text = "UND"
}
$var_txtSubID3.Text = Get-MediaInfoValue $FILE -Kind Text -Index 2 -Parameter 'Language/String'
$var_txtSubID3.Text = $ageList[$var_txtSubID3.Text]
if($var_txtSubID3.Text -eq ""){
$var_txtSubID3.Text = "UND"
}
$var_txtSubID4.Text = Get-MediaInfoValue $FILE -Kind Text -Index 3 -Parameter 'Language/String'
$var_txtSubID4.Text = $ageList[$var_txtSubID4.Text]
if($var_txtSubID4.Text -eq ""){
$var_txtSubID4.Text = "UND"
}
$var_txtSubID5.Text = Get-MediaInfoValue $FILE -Kind Text -Index 4 -Parameter 'Language/String'
$var_txtSubID5.Text = $ageList[$var_txtSubID5.Text]
if($var_txtSubID5.Text -eq ""){
$var_txtSubID5.Text = "UND"
}


#Titulky TrackName
$var_txtSubTRNameID1.Text = Get-MediaInfoValue $FILE -Kind Text -Index 0 -Parameter 'Title'
$var_txtSubTRNameID2.Text = Get-MediaInfoValue $FILE -Kind Text -Index 1 -Parameter 'Title'
$var_txtSubTRNameID3.Text = Get-MediaInfoValue $FILE -Kind Text -Index 2 -Parameter 'Title'
$var_txtSubTRNameID4.Text = Get-MediaInfoValue $FILE -Kind Text -Index 3 -Parameter 'Title'
$var_txtSubTRNameID5.Text = Get-MediaInfoValue $FILE -Kind Text -Index 4 -Parameter 'Title'




     #Subtiles ID SET VALUE
    if($var_txtSubTRNameID1.Text -eq "Forced" -or $var_txtSubTRNameID1.Text -eq "forced" ){
            $var_CboxSubTEnableID1.Text = "NO"
            $var_CboxSubforcedID1.Text = "YES"
            
    }

     if($var_txtSubTRNameID2.Text -eq "Forced" -or $var_txtSubTRNameID2.Text -eq "forced" ){
            $var_CboxSubTEnableID2.Text = "NO"
            $var_CboxSubforcedID2.Text = "YES"
            
    }

     if($var_txtSubTRNameID3.Text -eq "Forced" -or $var_txtSubTRNameID3.Text -eq "forced" ){
            $var_CboxSubTEnableID3.Text = "NO"
            $var_CboxSubforcedID3.Text = "YES"
            
    }

     if($var_txtSubTRNameID4.Text -eq "Forced" -or $var_txtSubTRNameID4.Text -eq "forced" ){
            $var_CboxSubTEnableID4.Text = "NO"
            $var_CboxSubforcedID4.Text = "YES"
            
    }

     if($var_txtSubTRNameID5.Text -eq "Forced" -or $var_txtSubTRNameID5.Text -eq "forced" ){
            $var_CboxSubTEnableID5.Text = "NO"
            $var_CboxSubforcedID5.Text = "YES"
            
    }

    $i++
    [int]$pct = ($i/$file.count)*100
    #update the progress bar
    $var_Progbar.Value = $pct
    $var_txtStatus.Text = "Informace byly načteny"  

}
}

}


function RENSUBTITLES {

$var_richbox.Document.blocks.Clear()
$var_Richbox.VerticalScrollBarVisibility = "Visible"
$var_console.IsSelected = $true
$var_txtStatus.Visibility = "Visible"

[System.Windows.Forms.Application]::DoEvents()
$Directory = $var_txtSetFolder.Text
Set-Location -path $Directory
$MKVMerge = $var_txtInputFilePath.Text
$FilesVideo = Get-ChildItem $var_txtSetFolder.Text -Recurse -File | where Length -gt 1024kb
$FilesMedia = Get-ChildItem $var_txtSetFolder.Text -Recurse -File | where Length -gt 1024kb | Select-Object -First 1
$VideoExtension = [System.IO.Path]::GetExtension($FilesVideo) | Select-Object -First 1
$Sub = Get-ChildItem $var_txtSetFolder.Text -Recurse -File | where Length -le 1024kb | Select-Object -First 1
$Subs = Get-ChildItem $var_txtSetFolder.Text -Recurse -File | where Length -le 1024kb 
$SubtitlesExtension = [System.IO.Path]::GetExtension($Sub)
    
    if($Subs.Count -lt $FilesVideo.Count -or $Subs.Count -gt $FilesVideo.Count -and $Subs.Count -ne "0" ){
    $oReturn=[System.Windows.Forms.MessageBox]::Show("Počet titulků neodpovídá počtu video souborů",[System.Windows.Forms.MessageBoxButtons]::"OK") 
    }Else{
    
    if($FilesVideo.Count -eq "0"){

    $oReturn=[System.Windows.Forms.MessageBox]::Show("Nebyli nalezeny žádné soubory ke zpracování",[System.Windows.Forms.MessageBoxButtons]::"OK")
     
    }Else{


    if($Directory -eq ""){
    $oReturn=[System.Windows.Forms.MessageBox]::Show("Není nastaven vstupní adresář",[System.Windows.Forms.MessageBoxButtons]::"OK")
    }Else{
       
    if($Subs.Count -eq "" ){
       
    }Else{
    $var_richbox.AppendText(("************************Přejmenování titulků*************************" | Out-String ))

[int]$Count = $Subs.Count - 1 

for ($i = 0; $i -le $Count; $i++ )
{
    
    $VideoOut = $FilesVideo[$i].basename
    $test = $VideoOut + $SubtitlesExtension   

    
    Rename-Item $Subs[$i] -NeWname $test
    $SubOut = $Subs[$i].basename
    $SubOrig = $SubOut + $SubtitlesExtension
         
    $mkvmergeoutput = "$i - $SubOrig byl přejmenována na $Test"
    $var_richbox.AppendText(($mkvmergeoutput | Out-String -Width 100))
    [System.Windows.Forms.Application]::DoEvents()
    
    

    }

  
   
}

 $var_richbox.AppendText(("************************Konec přejmenování titulků*************************" | Out-String ))



 }
}
}
}




Function MERGE{

$i=0
$var_txtStatus.Visibility = "Visible"
$Directory = $var_txtSetFolder.Text
Set-Location -path $Directory
$MKVMerge = $var_txtInputFilePath.Text
$FilesVideo = Get-ChildItem $var_txtSetFolder.Text -Recurse -File | where Length -gt 1024kb
$FilesMedia = Get-ChildItem $var_txtSetFolder.Text -Recurse -File | where Length -gt 1024kb | Select-Object -First 1
$VideoExtension = [System.IO.Path]::GetExtension($FilesVideo) | Select-Object -First 1
$Sub = Get-ChildItem $var_txtSetFolder.Text -Recurse -File | where Length -le 1024kb | Select-Object -First 1
$Subs = Get-ChildItem $var_txtSetFolder.Text -Recurse -File | where Length -le 1024kb 
$SubtitlesExtension = [System.IO.Path]::GetExtension($Sub)
$Euquals = [int]$FilesVideo.Count - [int]$Subs.Count

   
    if($Subs.Count -lt $FilesVideo.Count -or $Subs.Count -gt $FilesVideo.Count -and $Subs.Count -ne "0" ){
     
    }Else{
    
    if($FilesVideo.Count -eq "0"){

    
     
    }Else{


    if($Directory -eq ""){
    
    }Else{
       
    

  
    
    [int]$CountAudio = Get-MediaInfoValue $FilesMedia -Kind General -Parameter 'AudioCount'
    [int]$CountSubtitles = Get-MediaInfoValue $FilesMedia -Kind General -Parameter 'TextCount'
    $CountID =  $CountAudio + $CountSubtitles
    $pocet=1

    Foreach ($Files in $FilesVideo) {
    

    $RandomPASS = Get-RandomPassword 20
    
    #Get File Name
    $FormatName = $Files.ToString()
    $Name = $FormatName.Substring(0,$FormatName.Length-($VideoExtension.Length))
    $VideoExtension = $VideoExtension.ToString()
    $MKV = $Files.ToString()
    #Set Output File Name
    $Output = $Name + '___MERGED' + '.mkv'
    $Sub = $Name + $SubtitlesExtension


#Audio ID SET VALUE

    #TrackName ID SET VALUE

    $txtLangID1 = $var_txtLangID1.Text
    $txtLangID2 = $var_txtLangID2.Text
    $txtLangID3 = $var_txtLangID3.Text
    $txtLangID4 = $var_txtLangID4.Text
    $txtLangID5 = $var_txtLangID5.Text

    #TrackName ID SET VALUE

    $txtTrackNameID1 = $var_txtTrackNameID1.Text
    $txtTrackNameID2 = $var_txtTrackNameID2.Text
    $txtTrackNameID3 = $var_txtTrackNameID3.Text
    $txtTrackNameID4 = $var_txtTrackNameID4.Text
    $txtTrackNameID5 = $var_txtTrackNameID5.Text

    $CboxID1Track = $var_CboxID1Track.Text
    $CboxID2Track = $var_CboxID2Track.Text
    $CboxID3Track = $var_CboxID3Track.Text
    $CboxID4Track = $var_CboxID4Track.Text
    $CboxID5Track = $var_CboxID5Track.Text
    
    $CboxTEnableID1 = $var_CboxTEnableID1.Text
    $CboxTEnableID2 = $var_CboxTEnableID2.Text
    $CboxTEnableID3 = $var_CboxTEnableID3.Text
    $CboxTEnableID4 = $var_CboxTEnableID4.Text
    $CboxTEnableID5 = $var_CboxTEnableID5.Text

    $CboxID1Forced = $var_CboxID1Forced.Text
    $CboxID2Forced = $var_CboxID2Forced.Text
    $CboxID3Forced = $var_CboxID3Forced.Text
    $CboxID4Forced = $var_CboxID4Forced.Text
    $CboxID5Forced = $var_CboxID5Forced.Text

    $txtSubID1 = $var_txtSubID1.Text
    $txtSubID2 = $var_txtSubID2.Text
    $txtSubID3 = $var_txtSubID3.Text
    $txtSubID4 = $var_txtSubID4.Text
    $txtSubID5 = $var_txtSubID5.Text
    $txtSubID6 = $var_txtSubID6.Text

    $CboxSubID1Track = $var_CboxSubID1Track.Text
    $CboxSubID2Track = $var_CboxSubID2Track.Text
    $CboxSubID3Track = $var_CboxSubID3Track.Text
    $CboxSubID4Track = $var_CboxSubID4Track.Text
    $CboxSubID5Track = $var_CboxSubID5Track.Text
    $CboxSubID6Track = $var_CboxSubID6Track.Text
    
    $CboxSubTEnableID1 = $var_CboxSubTEnableID1.Text
    $CboxSubTEnableID2 = $var_CboxSubTEnableID2.Text
    $CboxSubTEnableID3 = $var_CboxSubTEnableID3.Text
    $CboxSubTEnableID4 = $var_CboxSubTEnableID4.Text
    $CboxSubTEnableID5 = $var_CboxSubTEnableID5.Text
    $CboxSubTEnableID6 = $var_CboxSubTEnableID6.Text

    $CboxSubforcedID1 = $var_CboxSubforcedID1.Text
    $CboxSubforcedID2 = $var_CboxSubforcedID2.Text
    $CboxSubforcedID3 = $var_CboxSubforcedID3.Text
    $CboxSubforcedID4 = $var_CboxSubforcedID4.Text
    $CboxSubforcedID5 = $var_CboxSubforcedID5.Text
    $CboxSubforcedID6 = $var_CboxSubforcedID6.Text

    #TrackName ID SET VALUE

    $txtSubTRNameID1 = $var_txtSubTRNameID1.Text
    $txtSubTRNameID2 = $var_txtSubTRNameID2.Text
    $txtSubTRNameID3 = $var_txtSubTRNameID3.Text
    $txtSubTRNameID4 = $var_txtSubTRNameID4.Text
    $txtSubTRNameID5 = $var_txtSubTRNameID5.Text
    $txtSubTRNameID6 = $var_txtSubTRNameID6.Text

    

    if ($Subs.Count -eq "0"){
        $Sub = "" }

   
    if($CountAudio -eq "1"){
    

    & $MKVMerge -o "$Output"  --track-name 0:$RandomPASS --language 0:UND --default-track 0:yes --forced-track 0:no  --track-name 1:$txtTrackNameID1 --language 1:$txtLangID1 --default-track 1:$CboxID1Track --track-enabled-flag 1:$CboxTEnableID1 --forced-display-flag 1:$CboxID1Forced --track-name 2:$txtSubTRNameID1 --language 2:$txtSubID1 --default-track 2:$CboxSubID1Track --track-enabled-flag 2:$CboxSubTEnableID1 --forced-display-flag 2:$CboxSubforcedID1 --track-name 3:$txtSubTRNameID2 --default-track 3:$CboxSubID2Track --track-enabled-flag 3:$CboxSubTEnableID2 --forced-display-flag 3:$CboxSubforcedID2 --track-name 4:$txtSubTRNameID3 --language 3:$txtSubID3 --default-track 4:$CboxSubID3Track --track-enabled-flag 4:$CboxSubTEnableID3 --forced-display-flag 4:$CboxSubforcedID3 --track-name 5:$txtSubTRNameID4 --language 5:$txtSubID4 --default-track 5:$CboxSubID4Track --track-enabled-flag 5:$CboxSubTEnableID4 --forced-display-flag 5:$CboxSubforcedID4 --track-name 6:$txtSubTRNameID5 --language 6:$txtSubID5 --default-track 6:$CboxSubID5Track --track-enabled-flag 6:$CboxSubTEnableID5 --forced-display-flag 6:$CboxSubforcedID5 --no-global-tags "$MKV" --track-name 0:$txtSubTRNameID6 --language 0:$txtSubID6 --default-track 0:$CboxSubID6Track --track-enabled-flag 0:$CboxSubTEnableID6 --forced-display-flag 0:$CboxSubforcedID6 "$Sub" # --default-track 0:$CboxSubID1Trac6 --track-enabled-flag 0:$CboxSubTEnableID6 --forced-display-flag 0:$CboxSubforcedID6 
    
    }
    
    if($CountAudio -eq "2"){
 
    
    & $MKVMerge -o "$Output "  --track-name 0:$RandomPASS --default-track 0:yes --forced-track 0:no  --track-name 1:$txtTrackNameID1 --language 1:$txtLangID1 --default-track 1:$CboxID1Track --track-enabled-flag 1:$CboxTEnableID1 --forced-display-flag 1:$CboxID1Forced --track-name 2:$txtTrackNameID2 --language 2:$txtLangID2 --default-track 2:$CboxID2Track --track-enabled-flag 2:$CboxTEnableID2 --forced-display-flag 2:$CboxID2Forced --track-name 3:$txtSubTRNameID1 --language 3:$txtSubID1 --default-track 3:$CboxSubID1Track --track-enabled-flag 3:$CboxSubTEnableID1 --forced-display-flag 3:$CboxSubforcedID1 --track-name 4:$txtSubTRNameID2 --language 4:$txtSubID2 --default-track 4:$CboxSubID2Track --track-enabled-flag 4:$CboxSubTEnableID2 --forced-display-flag 4:$CboxSubforcedID2 --track-name 5:$txtSubTRNameID3 --language 5:$txtSubID3 --default-track 5:$CboxSubID3Track --track-enabled-flag 5:$CboxSubTEnableID3 --forced-display-flag 5:$CboxSubforcedID3 --track-name 6:$txtSubTRNameID4 --language 6:$txtSubID4 --default-track 6:$CboxSubID4Track --track-enabled-flag 6:$CboxSubTEnableID4 --forced-display-flag 6:$CboxSubforcedID4 --track-name 7:$txtSubTRNameID5 --language 7:$txtSubID5 --default-track 7:$CboxSubID5Track --track-enabled-flag 7:$CboxSubTEnableID5 --forced-display-flag 7:$CboxSubforcedID5 --no-global-tags "$MKV" --track-name 0:$txtSubTRNameID6 --language 0:$txtSubID6 --default-track 0:$CboxSubID6Track --track-enabled-flag 0:$CboxSubTEnableID6 --forced-display-flag 0:$CboxSubforcedID6 "$Sub"
        
    }

    if($CountAudio -eq "3"){

    & $MKVMerge -o "$Output "  --track-name 0:$RandomPASS --default-track 0:yes --forced-track 0:no  --track-name 1:$txtTrackNameID1 --language 1:$txtLangID1 --default-track 1:$CboxID1Track --track-enabled-flag 1:$CboxTEnableID1 --forced-display-flag 1:$CboxID1Forced --track-name 2:$txtTrackNameID2 --language 2:$txtLangID2 --default-track 2:$CboxID2Track --track-enabled-flag 2:$CboxTEnableID2 --forced-display-flag 2:$CboxID2Forced --track-name 3:$txtTrackNameID3 --language 3:$txtLangID3 --default-track 3:$CboxID3Track --track-enabled-flag 3:$CboxTEnableID3 --forced-display-flag 3:$CboxID3Forced --track-name 4:$txtSubTRNameID1 --language 4:$txtSubID1 --default-track 4:$CboxSubID1Track --track-enabled-flag 4:$CboxSubTEnableID1 --forced-display-flag 4:$CboxSubforcedID1 --track-name 5:$txtSubTRNameID2 --language 5:$txtSubID2 --default-track 5:$CboxSubID2Track --track-enabled-flag 5:$CboxSubTEnableID2 --forced-display-flag 5:$CboxSubforcedID2 --track-name 6:$txtSubTRNameID3 --language 6:$txtSubID3 --default-track 6:$CboxSubID3Track --track-enabled-flag 6:$CboxSubTEnableID3 --forced-display-flag 6:$CboxSubforcedID3 --track-name 7:$txtSubTRNameID4 --language 7:$txtSubID4 --default-track 7:$CboxSubID4Track --track-enabled-flag 7:$CboxSubTEnableID4 --forced-display-flag 7:$CboxSubforcedID4 --track-name 8:$txtSubTRNameID5 --language 8:$txtSubID5 --default-track 8:$CboxSubID5Track --track-enabled-flag 8:$CboxSubTEnableID5 --forced-display-flag 8:$CboxSubforcedID5 --no-global-tags "$MKV" --track-name 0:$txtSubTRNameID6 --language 0:$txtSubID6 --default-track 0:$CboxSubID6Track --track-enabled-flag 0:$CboxSubTEnableID6 --forced-display-flag 0:$CboxSubforcedID6 "$Sub"
                  
    }

    if($CountAudio -eq "4"){
    
    & $MKVMerge -o "$Output "  --track-name 0:$RandomPASS --default-track 0:yes --forced-track 0:no  --track-name 1:$txtTrackNameID1 --language 1:$txtLangID1 --default-track 1:$CboxID1Track --track-enabled-flag 1:$CboxTEnableID1 --forced-display-flag 1:$CboxID1Forced --track-name 2:$txtTrackNameID2 --language 2:$txtLangID2 --default-track 2:$CboxID2Track --track-enabled-flag 2:$CboxTEnableID2 --forced-display-flag 2:$CboxID2Forced --track-name 3:$txtTrackNameID3 --language 3:$txtLangID3 --default-track 3:$CboxID3Track --track-enabled-flag 3:$CboxTEnableID3 --forced-display-flag 3:$CboxID3Forced --track-name 4:$txtTrackNameID4 --language 4:$txtLangID4 --default-track 4:$CboxID4Track --track-enabled-flag 4:$CboxTEnableID4 --forced-display-flag 4:$CboxID4Forced  --track-name 5:$txtSubTRNameID1 --language 5:$txtSubID1 --default-track 5:$CboxSubID1Track --track-enabled-flag 5:$CboxSubTEnableID1 --forced-display-flag 5:$CboxSubforcedID1 --track-name 6:$txtSubTRNameID2 --language 6:$txtSubID2 --default-track 6:$CboxSubID2Track --track-enabled-flag 6:$CboxSubTEnableID2 --forced-display-flag 6:$CboxSubforcedID2 --track-name 7:$txtSubTRNameID3 --language 7:$txtSubID3 --default-track 7:$CboxSubID3Track --track-enabled-flag 7:$CboxSubTEnableID3 --forced-display-flag 7:$CboxSubforcedID3 --track-name 8:$txtSubTRNameID4 --language 8:$txtSubID4 --default-track 8:$CboxSubID4Track --track-enabled-flag 8:$CboxSubTEnableID4 --forced-display-flag 8:$CboxSubforcedID4 --track-name 9:$txtSubTRNameID5 --language 9:$txtSubID5 --default-track 9:$CboxSubID5Track --track-enabled-flag 9:$CboxSubTEnableID5 --forced-display-flag 9:$CboxSubforcedID5 "$MKV" --track-name 0:$txtSubTRNameID6 --language 0:$txtSubID6 --default-track 0:$CboxSubID6Track --track-enabled-flag 0:$CboxSubTEnableID6 --forced-display-flag 0:$CboxSubforcedID6 "$Sub"
       
    }

    if($CountAudio -eq "5"){

    & $MKVMerge -o "$Output "  --track-name 0:$RandomPASS --default-track 0:yes --forced-track 0:no  --track-name 1:$txtTrackNameID1 --language 1:$txtLangID1 --default-track 1:$CboxID1Track --track-enabled-flag 1:$CboxTEnableID1 --forced-display-flag 1:$CboxID1Forced --track-name 2:$txtTrackNameID2 --language 2:$txtLangID2 --default-track 2:$CboxID2Track --track-enabled-flag 2:$CboxTEnableID2 --forced-display-flag 2:$CboxID2Forced --track-name 3:$txtTrackNameID3 --language 3:$txtLangID3 --default-track 3:$CboxID3Track --track-enabled-flag 3:$CboxTEnableID3 --forced-display-flag 3:$CboxID3Forced --track-name 4:$txtTrackNameID4 --language 4:$txtLangID4 --default-track 4:$CboxID4Track --track-enabled-flag 4:$CboxTEnableID4 --forced-display-flag 4:$CboxID4Forced --track-name 5:$txtTrackNameID5 --language 5:$txtLangID5 --default-track 5:$CboxID5Track --track-enabled-flag 5:$CboxTEnableID5 --forced-display-flag 5:$CboxID5Forced  --track-name 6:$txtSubTRNameID1 --language 6:$txtSubID1 --default-track 6:$CboxSubID1Track --track-enabled-flag 6:$CboxSubTEnableID1 --forced-display-flag 6:$CboxSubforcedID1 --track-name 7:$txtSubTRNameID2 --language 7:$txtSubID2 --default-track 7:$CboxSubID2Track --track-enabled-flag 7:$CboxSubTEnableID2 --forced-display-flag 7:$CboxSubforcedID2 --track-name 8:$txtSubTRNameID3 --language 8:$txtSubID3 --default-track 8:$CboxSubID3Track --track-enabled-flag 8:$CboxSubTEnableID3 --forced-display-flag 8:$CboxSubforcedID3 --track-name 9:$txtSubTRNameID4 --language 9:$txtSubID4 --default-track 9:$CboxSubID4Track --track-enabled-flag 9:$CboxSubTEnableID4 --forced-display-flag 9:$CboxSubforcedID4 --track-name 10:$txtSubTRNameID5 --language 10:$txtSubID5 --default-track 10:$CboxSubID5Track --track-enabled-flag 10:$CboxSubTEnableID5 --forced-display-flag 10:$CboxSubforcedID5 "$MKV" --track-name 0:$txtSubTRNameID6 --language 0:$txtSubID6 --default-track 0:$CboxSubID6Track --track-enabled-flag 0:$CboxSubTEnableID6 --forced-display-flag 0:$CboxSubforcedID6 "$Sub"
         
    } 
   
    if(-not $var_chkcOrigin.IsChecked){
            
            
            if($Subs.Count -gt 0){
               
               Remove-Item $Sub
            }
            
            
            
            Remove-Item $MKV
            Rename-Item $Output -NewName ($Name + '.mkv')

           
    }
    
    
    if($pocet -eq "1"){
    $mkvmergeoutput = ""
    $var_richbox.AppendText(("************************ Merge*************************" | Out-String ))
    $var_richbox.AppendText(($mkvmergeoutput | Out-String -Width 3))
    [System.Windows.Forms.Application]::DoEvents()
    #$var_richbox.Document.blocks.Clear()
    }
    
    $mkvmergeoutput = "$pocet - $MKV byl zpracován."
    $var_richbox.AppendText(($mkvmergeoutput | Out-String -Width 3))
    [System.Windows.Forms.Application]::DoEvents()
    $pocet++
    $i++
    
    [int]$pct = ($i/$FilesVideo.count)*100
    #update the progress bar
    $var_Progbar.Value = $pct
    $var_txtStatus.Text = "$PCT %"
    [System.Windows.Forms.Application]::DoEvents()
    }
    
    $var_richbox.AppendText(("********************Merge byl dokončen*******************"| Out-String ))
    $var_txtStatus.Text = "!!HOTOVO!!"
    [System.Windows.Forms.Application]::DoEvents()
    }
    
        
}
}
}
#}       

    

#Funkce Get-Randomword náhodné heslo
function Get-RandomPassword {
    param (
        [Parameter(Mandatory)]
        [int] $length,
        [int] $amountOfNonAlphanumeric = 1
    )
    Add-Type -AssemblyName 'System.Web'
    return [System.Web.Security.Membership]::GeneratePassword($length, $amountOfNonAlphanumeric)
}
$RandomPASS = Get-RandomPassword 20


function HASH{
$var_richbox.Document.blocks.Clear()
$var_console.IsSelected = $true
$i=0
$var_txtStatus.Visibility = "Visible"
$Directory = $var_txtSetFolder.Text
Set-Location -path $Directory
$MKVMerge = $var_txtInputFilePath.Text
$FilesVideo = Get-ChildItem $var_txtSetFolder.Text -Recurse -File | where Length -gt 1024kb
$FilesMedia = Get-ChildItem $var_txtSetFolder.Text -Recurse -File | where Length -gt 1024kb | Select-Object -First 1
$VideoExtension = [System.IO.Path]::GetExtension($FilesVideo) | Select-Object -First 1
$Sub = Get-ChildItem $var_txtSetFolder.Text -Recurse -File | where Length -le 1024kb | Select-Object -First 1
$Subs = Get-ChildItem $var_txtSetFolder.Text -Recurse -File | where Length -le 1024kb 
$SubtitlesExtension = [System.IO.Path]::GetExtension($Sub)
$Euquals = [int]$FilesVideo.Count - [int]$Subs.Count


    if($Directory -eq ""){
    $oReturn=[System.Windows.Forms.MessageBox]::Show("Není nastaven vstupní adresář",[System.Windows.Forms.MessageBoxButtons]::"OK")
    }Else{   
    
    
    if($Subs.Count -lt $FilesVideo.Count -or $Subs.Count -gt $FilesVideo.Count -and $Subs.Count -ne "0" ){
    $oReturn=[System.Windows.Forms.MessageBox]::Show("Počet titulků neodpovídá počtu video souborů",[System.Windows.Forms.MessageBoxButtons]::"OK") 
    }Else{

  
    
    [int]$CountAudio = Get-MediaInfoValue $FilesMedia -Kind General -Parameter 'AudioCount'
    [int]$CountSubtitles = Get-MediaInfoValue $FilesMedia -Kind General -Parameter 'TextCount'
    $CountID =  $CountAudio + $CountSubtitles
    $pocet=0

    Foreach ($Files in $FilesVideo) {
    

    $RandomPASS = Get-RandomPassword 20
    
    #Get File Name
    $FormatName = $Files.ToString()
    $Name = $FormatName.Substring(0,$FormatName.Length-($VideoExtension.Length))
    $VideoExtension = $VideoExtension.ToString()
    $MKV = $Files.ToString()
    #Set Output File Name
    $Output = $Name + '___MERGED' + '.mkv'
    $Sub = $Name + $SubtitlesExtension


#Audio ID SET VALUE

    #TrackName ID SET VALUE

    $txtLangID1 = $var_txtLangID1.Text
    $txtLangID2 = $var_txtLangID2.Text
    $txtLangID3 = $var_txtLangID3.Text
    $txtLangID4 = $var_txtLangID4.Text
    $txtLangID5 = $var_txtLangID5.Text

    #TrackName ID SET VALUE

    $txtTrackNameID1 = $var_txtTrackNameID1.Text
    $txtTrackNameID2 = $var_txtTrackNameID2.Text
    $txtTrackNameID3 = $var_txtTrackNameID3.Text
    $txtTrackNameID4 = $var_txtTrackNameID4.Text
    $txtTrackNameID5 = $var_txtTrackNameID5.Text

    $CboxID1Track = $var_CboxID1Track.Text
    $CboxID2Track = $var_CboxID2Track.Text
    $CboxID3Track = $var_CboxID3Track.Text
    $CboxID4Track = $var_CboxID4Track.Text
    $CboxID5Track = $var_CboxID5Track.Text
    
    $CboxTEnableID1 = $var_CboxTEnableID1.Text
    $CboxTEnableID2 = $var_CboxTEnableID2.Text
    $CboxTEnableID3 = $var_CboxTEnableID3.Text
    $CboxTEnableID4 = $var_CboxTEnableID4.Text
    $CboxTEnableID5 = $var_CboxTEnableID5.Text

    $CboxID1Forced = $var_CboxID1Forced.Text
    $CboxID2Forced = $var_CboxID2Forced.Text
    $CboxID3Forced = $var_CboxID3Forced.Text
    $CboxID4Forced = $var_CboxID4Forced.Text
    $CboxID5Forced = $var_CboxID5Forced.Text

    $txtSubID1 = $var_txtSubID1.Text
    $txtSubID2 = $var_txtSubID2.Text
    $txtSubID3 = $var_txtSubID3.Text
    $txtSubID4 = $var_txtSubID4.Text
    $txtSubID5 = $var_txtSubID5.Text

    $CboxSubID1Track = $var_CboxSubID1Track.Text
    $CboxSubID2Track = $var_CboxSubID2Track.Text
    $CboxSubID3Track = $var_CboxSubID3Track.Text
    $CboxSubID4Track = $var_CboxSubID4Track.Text
    $CboxSubID5Track = $var_CboxSubID5Track.Text
    
    $CboxSubTEnableID1 = $var_CboxSubTEnableID1.Text
    $CboxSubTEnableID2 = $var_CboxSubTEnableID2.Text
    $CboxSubTEnableID3 = $var_CboxSubTEnableID3.Text
    $CboxSubTEnableID4 = $var_CboxSubTEnableID4.Text
    $CboxSubTEnableID5 = $var_CboxSubTEnableID5.Text

    $CboxSubforcedID1 = $var_CboxSubforcedID1.Text
    $CboxSubforcedID2 = $var_CboxSubforcedID2.Text
    $CboxSubforcedID3 = $var_CboxSubforcedID3.Text
    $CboxSubforcedID4 = $var_CboxSubforcedID4.Text
    $CboxSubforcedID5 = $var_CboxSubforcedID5.Text

    #TrackName ID SET VALUE

    $txtSubTRNameID1 = $var_txtSubTRNameID1.Text
    $txtSubTRNameID2 = $var_txtSubTRNameID2.Text
    $txtSubTRNameID3 = $var_txtSubTRNameID3.Text
    $txtSubTRNameID4 = $var_txtSubTRNameID4.Text
    $txtSubTRNameID5 = $var_txtSubTRNameID5.Text

    

    if ($Subs.Count -eq "0"){
        $Sub = "" }

   
    if($CountAudio -eq "1"){
    

    & $MKVMerge -o "$Output"  --default-track 0:yes --forced-track 0:no --no-global-tags "$MKV"
    
    }
    
    if($CountAudio -eq "2"){
   
    
    & $MKVMerge -o "$Output"  --track-name 0:$RandomPASS --default-track 0:yes --forced-track 0:no --no-global-tags "$MKV"
        
    }

    if($CountAudio -eq "3"){

    & $MKVMerge -o "$Output "  --track-name 0:$RandomPASS --default-track 0:yes --forced-track 0:no --no-global-tags "$MKV"  
                  
    }

    if($CountAudio -eq "4"){
    
    & $MKVMerge -o "$Output "  --track-name 0:$RandomPASS --default-track 0:yes --forced-track 0:no --no-global-tags "$MKV" 
       
    }

    if($CountAudio -eq "5"){

    & $MKVMerge -o "$Output "  --track-name 0:$RandomPASS --default-track 0:yes --forced-track 0:no --no-global-tags "$MKV" 
         
    } 
   
    if(-not $var_chkcOrigin.IsChecked){
            
              
            
            Remove-Item $MKV
            Rename-Item $Output -NewName ($Name + '.mkv')

           
    }
    
    
    if($pocet -eq "1"){
    $mkvmergeoutput = ""
    $var_richbox.AppendText(($mkvmergeoutput | Out-String -Width 3))
    [System.Windows.Forms.Application]::DoEvents()
    #$var_richbox.Document.blocks.Clear()
    }
    
    $mkvmergeoutput = "$pocet - $MKV byl zpracován."
    $var_richbox.AppendText(($mkvmergeoutput | Out-String -Width 3))
    [System.Windows.Forms.Application]::DoEvents()
    $pocet++
    $i++
    
    [int]$pct = ($i/$FilesVideo.count)*100
    #update the progress bar
    $var_Progbar.Value = $pct
    $var_txtStatus.Text = "$PCT %"
    [System.Windows.Forms.Application]::DoEvents()
    }
    
    $var_richbox.AppendText("Multiplexing byl dokončen, přeji krásný den")
    $var_txtStatus.Text = "!!HOTOVO!!"
    }
    
}
}







#Přejmenování souborů
function rename-ws {

    $Directory = $var_txtSetFolder.Text
    #if($Subs.Count -lt $FilesVideo.Count -or $Subs.Count -gt $FilesVideo.Count -and $Subs.Count -ne "0" ){
     
    #}Else{
    
    #if($FilesVideo.Count -eq "0"){

    
     
    #}Else{

    
    if($Directory -eq ""){
    
    }Else{

    [System.Windows.Forms.Application]::DoEvents()
    $var_richbox.AppendText("********************Přejmenování souborů*******************")
    
    Set-Location -path $Directory
    $random = Get-Random -Maximum 10000000000000000
    $MKV1=Get-ChildItem $Directory -Name *MERGED*  
    $MKV=Get-ChildItem $Directory -Name *MERGED* | Select-Object -First 9 
    $MKVALL= Get-ChildItem $Directory -Name *MERGED* -exclude $MKV
    $VideoExtension = [System.IO.Path]::GetExtension($MKV) | Select-Object -First 1
    $Files = Get-ChildItem $Directory -exclude $MKVALL

    $HASH = Get-ChildItem $Directory -Recurse -File | where Length -gt 1024kb | Select-Object -First 9
    $HASH10 = Get-ChildItem $Directory -Recurse -File -Exclude $HASH | where Length -gt 1024kb 
    $HASExtension = [System.IO.Path]::GetExtension($HASH) | Select-Object -First 1
    Write-Host 
    
    
    $pocet=1
    [System.Windows.Forms.Application]::DoEvents()
    $var_richbox.AppendText(("" | Out-String ))
    Write-host $MKV1.Count

    if($MKV1.Count -eq '0' -and -not $var_chkcOrigin.IsChecked){
    Write-Host "Nastavuju is Merged 0 and not checked"
    
    }
    
    if($MKV1.Count -gt 0 -and $var_chkcOrigin.IsChecked){ 
    $HASH = $MKV=Get-ChildItem $Directory -Name *MERGED* | Select-Object -First 9
    }
   
    $count = 1
    

    Foreach ($File in $HASH) {
    $random = Get-Random -Maximum 10000000000000000   
    $OutName="0$count$Random"
    Rename-Item $File -NewName $Outname
    $count++ 
    
    $mkvmergeoutput = "$pocet - Soubor $File byl přejmenován na $OutName."
    $var_richbox.AppendText(($mkvmergeoutput | Out-String ))
    [System.Windows.Forms.Application]::DoEvents()
    $pocet++
    $i++
     
    }


    if(-not $MKV1.Count -gt 0 -and $var_chkcOrigin.IsChecked){ 
    $HASH = $MKV=Get-ChildItem $Directory -Name *.$HASExtension} 
    
    if($MKV1.Count -gt 0 -and $var_chkcOrigin.IsChecked){ 
    $HASH10 = Get-ChildItem $Directory -Name *MERGED* }

    $pocet=10
    $OutName="$pocet$Random"  
    Write-Host "HASH10:"$HASH10    
    Foreach ($File in $HASH10) { 
    $mkvmergeoutput = "$pocet - Soubor $File byl přejmenován na $Outname."
    
    $random = Get-Random -Maximum 10000000000000000     
    Rename-Item $File -NewName "$pocet$Random"
    $var_richbox.AppendText(($mkvmergeoutput | Out-String ))
    [System.Windows.Forms.Application]::DoEvents()
    $pocet++

}
}
}







$var_btn_mkvmerge.Add_Click({
    $InputFilePick=New-Object System.Windows.Forms.OpenFileDialog
    $InputFilePick.ShowDialog()
    $var_txtInputFilePath.Text=$InputFilePick.FileName

})



$var_btnSetFolder.Add_Click({
   

    
    
    $SubtitlesExtension = [System.IO.Path]::GetExtension($Subtitles)
    $InputFolder=New-Object System.Windows.Forms.FolderBrowserDialog
    $InputFolder.ShowDialog()
    $var_txtSetFolder.Text=$InputFolder.SelectedPath
    $Files = Get-ChildItem $InputFolder.SelectedPath -Recurse -File | where Length -gt 1024kb
    $Subs = Get-ChildItem  $InputFolder.SelectedPath -Recurse -File | where Length -le 1024kb
    $Directory = $var_txtSetFolder.Text
    $var_txtNumberFile.Text = $Files.Count
    $var_txtNumberSubs.Text = $Subs.Count
    $var_ProgBar.Value = 0
    $var_txtStatus.Text = ""
    

    
    

})


$var_btnMult1.Add_Click({

    
    RENSUBTITLES
    MERGE
    
    
    if($var_txtNumberFile.Text -eq ""){
    }Else{   

    
    $oReturn=[System.Windows.Forms.MessageBox]::Show("Vaše soubory byly zpracovány")
    $var_ProgBar.Value = 0
    $var_txtStatus.Text = ""
    }


})

$var_btnMult2.Add_Click({

    RENSUBTITLES
    MERGE
    RENAME-WS
    

    if($var_txtNumberFile.Text -eq ""){
    }Else{   

       

    $oReturn=[System.Windows.Forms.MessageBox]::Show("Vaše soubory byly zpracovány")
    $var_ProgBar.Value = 0
    $var_txtStatus.Text = ""
    }

})


$var_btnMult3.Add_Click({

    HASH
    RENAME-WS

    if($var_txtNumberFile.Text -eq ""){
    }Else{   

  

    $oReturn=[System.Windows.Forms.MessageBox]::Show("Vaše soubory byly zpracovány")
    $var_ProgBar.Value = 0
    $var_txtStatus.Text = ""
    }
    
    
    
})


$var_btnMediaInfo.Add_Click({

    
    MEDIAINFO
    
        
})





$psform.ShowDialog()
